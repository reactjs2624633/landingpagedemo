import ImgCard from "./ImgCard";

import profilePic1 from "../assets/home/profile1.jpg";
import profilePic2 from "../assets/home/profile2.jpg";
import profilePic3 from "../assets/home/profile3.jpg";
import profilePic4 from "../assets/home/profile4.jpg";
import profilePic5 from "../assets/home/profile5.jpg";
import profilePic6 from "../assets/home/profile6.jpg";

const Team = () => {
  const teamMembers = [
    {
      profilePic: profilePic1,
      name: "Connor Quinn",
      position: "President, CEO",
    },

    {
      profilePic: profilePic2,
      name: "Hunter Norton",
      position: "HR Manager",
    },

    {
      profilePic: profilePic3,
      name: "Betty Nilson",
      position: "Chief Accountant",
    },

    {
      profilePic: profilePic4,
      name: "Samuel Schick",
      position: "Sales Manager",
    },

    {
      profilePic: profilePic5,
      name: "Joe Alvarez",
      position: "Sales Manager",
    },

    {
      profilePic: profilePic6,
      name: "Roxie Swanson",
      position: "Vice President",
    },
  ];

  return (
    <section className="container team-sec padding-container text-center">
      <h1 className="text-primary fw-bold heading">Our Team</h1>

      <p className="fs-5 lh-base fst-italic mt-4">
        Our methodical approach provides not only effective solutions, but we
        also provide creative answers and unparalleled services to challenges
        faced by our clients.
      </p>

      <div className="row mt-2 mt-md-4">
        {teamMembers.map((member) => (
          <div
            key={member.name}
            className="col-12 col-md-4 gy-5 gx-2 mb-2 mb-md-5"
          >
            <ImgCard {...member} />
          </div>
        ))}
      </div>
    </section>
  );
};

export default Team;
