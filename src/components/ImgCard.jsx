/* eslint-disable react/prop-types */
const ImgCard = ({ profilePic, name, position }) => {
  return (
    <div>
      <div>
        <img
          src={profilePic}
          alt=""
          style={{
            width: "330px",
            height: "355px",
            objectFit: "cover",
            objectPosition: "top center",
          }}
        />
      </div>
      <h4 className="text-primary my-3">{name}</h4>
      <h5 className="fw-normal">{position}</h5>
    </div>
  );
};

export default ImgCard;
