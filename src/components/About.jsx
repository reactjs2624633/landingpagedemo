const About = () => {
  return (
    <section className="about-sec container text-center">
      <h1 className="text-primary fw-bold heading">Exclusive desserts</h1>
      <div className="underline bg-primary mx-auto my-4"></div>
      <p className="fs-5 lh-base fw-semibold">
        Paragraph. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Curabitur id suscipit ex. Suspendisse rhoncus laoreet purus quis
        elementum. Phasellus sed efficitur dolor, et ultricies sapien. Quisque
        fringilla sit amet dolor commodo efficitur. Aliquam et sem odio. In
        ullamcorper nisi nunc, et molestie ipsum iaculis sit amet.
      </p>
    </section>
  );
};

export default About;
