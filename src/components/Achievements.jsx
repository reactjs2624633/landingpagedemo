/* eslint-disable no-unused-vars */
import Box from "./Box";

const Achievements = () => {
  const items = [
    {
      numbers: "1000+",
      title: "TEMPLATES",
      gradient: "linear-gradient(45deg, #62f6e9, #b48dff)",
      content:
        "Sample text. Lorem ipsum dolor sit amet, consectetur dipiscing elit nullam nunc justo sagittis suscipit ultrices.",
    },

    {
      numbers: "100k",
      title: "HAPPY USERS",
      gradient: "linear-gradient(-45deg, #4284ff, #62f6e9)",
      content:
        "Sample text. Lorem ipsum dolor sit amet, consectetur dipiscing elit nullam nunc justo sagittis suscipit ultrices.",
    },

    {
      numbers: "20+",
      title: "EXPERIENCE",
      gradient: "linear-gradient(-45deg, #ec6693, #b48dff)",
      content:
        "Sample text. Lorem ipsum dolor sit amet, consectetur dipiscing elit nullam nunc justo sagittis suscipit ultrices.",
    },
  ];

  return (
    <div className="achievements">
      <div className="container row gx-4 gy-4">
        {items.map((item, index) => (
          <div key={index} className="col-12 col-md-4">
            <Box {...item} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Achievements;
