import img1 from "../assets/home/mission1.jpg";
import img2 from "../assets/home/mission2.jpg";
import icon1 from "../assets/home/lightbulb.png";
import icon2 from "../assets/home/globe.png";

const Mission = () => {
  return (
    <section className="mission-sec">
      <div className="container padding-container mission-content">
        <div className="row g-0">
          <div className="missionBg-container col-12 col-md-6 p-4 text-white d-flex align-items-center justify-content-center flex-column">
            <h1 className="fw-bold heading">Boost Your Business Now</h1>
            <p className="fs-5 lh-base fw-semibold mt-4">
              We are creative agency, composed by a unique team of qualified and
              experienced professionals, dedicated to Graphic and Web Design,
              Internet, Audiovisual and Multimedia, Advertising and Marketing.
            </p>
          </div>

          <div className="col-12 col-md-6 overflow-hidden text-center">
            <div className="row g-0">
              <div className="col-12 col-md-6 mission-icon d-flex flex-column align-items-center justify-content-center gap-4 order-2 order-md-1 bg-white">
                <img src={icon1} alt="" />
                <h2 className="text-primary">Our Mission</h2>
              </div>
              <div
                className="col-6 mission-img order-1 order-md-2"
                style={{
                  backgroundImage: `url(${img2})`,
                }}
              ></div>
            </div>

            <div className="row g-0">
              <div
                className="col-6 mission-img"
                style={{
                  backgroundImage: `url(${img1})`,
                }}
              ></div>
              <div className="col-12 col-md-6 mission-icon d-flex flex-column align-items-center justify-content-center gap-4 bg-white">
                <img src={icon2} alt="" />
                <h2 className="text-primary">Perfect Solution</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Mission;
