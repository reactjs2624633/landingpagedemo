/* eslint-disable react/prop-types */
const Box = ({ numbers, title, gradient, content }) => {
  return (
    <section className="achievements-box text-center">
      <div
        className="achievements-box-content py-4 text-white"
        style={{ background: gradient }}
      >
        <h1>{numbers}</h1>
        <div className="achievement-hr"></div>
        <h4>{title}</h4>
      </div>

      <div className="bg-white py-5 px-3">
        <p className="fst-italic fs-5">{content}</p>
        <button className="btn btn-primary rounded-0 px-4">more</button>
      </div>
    </section>
  );
};

export default Box;
