const Form = () => {
  return (
    <form className="d-flex flex-column align-items-center justify-content-center gap-3">
      <input type="text" placeholder="Name" />
      <input type="email" placeholder="Email" />
      <textarea cols="30" rows="4" placeholder="Message"></textarea>
      <div className="d-flex justify-content-start w-100">
        <button
          className="border-0 px-5 py-2 fw-semibold text-white fs-5"
          style={{ background: "#b48dff" }}
        >
          SUBMIT
        </button>
      </div>
    </form>
  );
};

export default Form;
