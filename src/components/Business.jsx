import clientPic1 from "../assets/home/client1.jpg";
import clientPic2 from "../assets/home/client2.jpg";
import clientPic3 from "../assets/home/client3.jpg";

const Business = () => {
  const clients = [
    {
      id: "1",
      photo: clientPic1,
    },

    {
      id: "2",
      photo: clientPic2,
    },

    {
      id: "3",
      photo: clientPic3,
    },
  ];

  return (
    <section className="business-sec text-white">
      <div className="container padding-container business-content text-center">
        <h1 className="fw-bold heading">Business And Marketing</h1>

        <p className="fs-5 lh-base mt-4">
          ullam mauris nibh, pellentesque quis tincidunt eu, tincidunt sed
          nulla, sit amet tristique elit finibus.
        </p>

        <div className="row mt-3 mt-md-5">
          {clients.map((client) => (
            <div
              key={client.id}
              className="col-12 col-md-4 gy-2 gx-4 mb-2 mb-md-5"
            >
              <img
                src={client.photo}
                alt=""
                style={{ width: "100%", height: "400px", objectFit: "cover" }}
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Business;
