const Banner = () => {
  return (
    <section className="banner banner-bg">
      <div className="container banner-content padding-container z-3">
        <h1 className="text-white fw-normal">
          The perfect design{" "}
          <b style={{ fontFamily: "Roboto" }}>for your website</b>
        </h1>

        <hr className="banner-hr" />

        <p className="fs-3 text-white">
          Free website builder. Mobile-friendly. No coding Joomla, WordPress,
          HTML, Windows, Mac OS and online
        </p>

        <button className="bannerBtn">TRY NOW</button>
      </div>
    </section>
  );
};

export default Banner;
