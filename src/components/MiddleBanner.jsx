import img1 from "../assets/home/middle1.jpg";
import img2 from "../assets/home/middle2.jpg";

const MiddleBanner = () => {
  return (
    <section className="middle-banner-sec">
      <div className="container padding-container">
        <div className="row">
          <div className="col-12 col-md-4 p-0">
            <img src={img1} alt="" />
          </div>

          <div className="col-12 col-md-4 bg-white d-flex align-items-center justify-content-center flex-column g-0 px-2 px-md-4 py-5">
            <h2 className="fw-bold mb-4">
              The justice shall be the highest law
            </h2>
            <p className="fs-5">
              Phasellus scelerisque sed leo quis gravida. Fusce lobortis libero
              ut arcu blandit pharetra.
            </p>
          </div>

          <div className="col-12 col-md-4 p-0">
            <img src={img2} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default MiddleBanner;
