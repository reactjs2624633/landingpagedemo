import Form from "./Form";

const Contact = () => {
  return (
    <section className="contact-sec bg-primary text-center d-flex flex-column align-items-center justify-content-center mx-auto my-5 text-white">
      <h1 className="mb-3 mb-md-4 fw-bold heading">Empire State Building</h1>
      <p className="fs-5">350 5th Ave, New York, NY 10118</p>
      <p className="fs-5 py-0 py-md-2">1 212-736-3100</p>
      <p className="fs-5">contacts@esbnyc.com</p>

      <Form />
    </section>
  );
};

export default Contact;
