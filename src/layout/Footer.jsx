import Facebook from "../assets/home/facebook.png";
import Twitter from "../assets/home/twitter.png";
import Instagram from "../assets/home/instagram.png";
import LinkedIn from "../assets/home/linkedin.png";

const Footer = () => {
  const icons = [
    {
      id: "1",
      iconImg: Facebook,
    },

    {
      id: "2",
      iconImg: Twitter,
    },

    {
      id: "3",
      iconImg: Instagram,
    },

    {
      id: "4",
      iconImg: LinkedIn,
    },
  ];

  return (
    <footer className="bg-dark">
      <div className="footer-sec padding-container row container text-white mx-auto gy-5">
        <div className="col-12 col-md-3">
          <h5 className="mb-2">Headline</h5>
          <p>Sample footer text</p>
        </div>

        <div className="col-12 col-md-3">
          <h5 className="mb-2">Headline</h5>
          <p>Sample footer text</p>
        </div>

        <div className="col-12 col-md-3">
          <h5 className="mb-2">Headline</h5>
          <p>Sample footer text</p>
        </div>

        <div className="col-12 col-md-3">
          <h5 className="mb-2">Headline</h5>
          <p>Sample footer text</p>
        </div>
      </div>

      <div className="container padding-container d-flex justify-content-end justify-content-md-center gap-3 mt-5 pb-5">
        {icons.map((icon) => (
          <img
            key={icon.id}
            src={icon.iconImg}
            alt=""
            style={{
              width: "15px",
              height: "15px",
              objectFit: "contain",
              cursor: "pointer",
            }}
          />
        ))}
      </div>
    </footer>
  );
};

export default Footer;
