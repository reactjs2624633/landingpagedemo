import About from "../components/About";
import Achievements from "../components/Achievements";
import Banner from "../components/Banner";
import Business from "../components/Business";
import Contact from "../components/Contact";
import MiddleBanner from "../components/MiddleBanner";
import Mission from "../components/Mission";
import Team from "../components/Team";

const Home = () => {
  return (
    <div>
      <Banner />
      <Achievements />
      <About />
      <Mission />
      <Team />
      <MiddleBanner />
      <Contact />
      <Business />
    </div>
  );
};

export default Home;
